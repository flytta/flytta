# Flytta
Flytta is a free and open source frame-by-frame animation program.

> If you're reading this through GitHub, our main repository is at [Gitdab](https://gitdab.com/flytta/flytta). The mirror is read-only, so if you wanna open an issue, or a pull request, go to the main repo.
### Documentation
Documentation on how to use/build/contribute to Flytta etc., is listed in the wiki of this repo **and** is also listed on our website at https://docs.flytta.org/home.
### License
Flytta is licensed under the Apache License, version 2.0. For more information, [read the LICENSE file here](https://gitdab.com/Flytta/flytta/src/branch/master/LICENSE).